function [G_linear]    = channelgain_cost231(distance,frequency,hbs,hms)

%   This very simple function computes the channel gain (linear) for one user 

%   Inputs :
%   distance : distance between BS and MS
%   frequency :central frequency of the band (f)
%   hbs : height of the base station
%   hms : height of the mobile station
%
%   Outputs :
%   PL_linear : linear value of the pathloss

%   Other m-files required: none
%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

% This function is used by the file main_macro.m
% see: http://grow.tecnico.ulisboa.pt/~grow.daemon/cost231/

%   Author  : R�mi BONNEFOI
%   SCEE Research Team - CentraleSup�lec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%
%   Email : remi.bonnefoi@centralesupelec.fr
%   Website : remibonnefoi.wordpress.com
%
%   Last revision : 07/27/2016

% computation of the path loss in dB

a           = (1.1*log10(frequency)-0.7)*hms-(1.56*log10(frequency)-0.8);

PL_dB       = 46.3+33.9*log10(frequency)-13.82*log10(hbs)-a+(44.9-6.55*log10(hbs))*log10(distance/1000);

G               = 10; % antenna gain dBi

SD              = normrnd(0,8); % standard deviation

NF              = 9; % noise figure

%thermal noise
N               = -174;

channel_gain    = G-PL_dB-SD-(N+NF);

% channel gain
G_linear        = 10^(channel_gain/10);
end