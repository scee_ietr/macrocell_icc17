function [mu_opt,nb_iter] = opt_sol_Newton(Nu,mu_sopt,mu_min,B,cap_const,G,m,Pmax,P0,Ps,err)

%   OPT_SOL_NEWTON : This function computes the optimal values of 
%   mu by computing the optimum when the sum is equal to 1        
%   with the Newton's method                                     

%   Inputs :                                                    
%   Nu                : Number of users                          
%   mu_sopt           : Mu_opt wihout constraints                
%   mu_min            : Minimum acceptable value for mu_opt      
%   B                 : band                                     
%   cap_const         : capcity constraints                      
%   G                 : vector of channel gains                   
%   m                 : slope of the power supply                 
%   Pmax              : maximum transmit power                   
%   P0                : Static power while transmitting          
%   Ps                : Sleep mode power consumption             
%   err               : error accepted with the Newton method    
%                     all constraints  

%   Outputs :                                                    
%   mu_opt            : Optimal value of mu_opt                        
%   nb_iter           : number of iterations                     

%   Other m-files required: none
%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

% This function is used by the file main_macro.m

%   Author  : R�mi BONNEFOI
%   SCEE Research Team - CentraleSup�lec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%
%   Email : remi.bonnefoi@centralesupelec.fr
%   Website : remibonnefoi.wordpress.com
%
%   Last revision : 07/27/2016



% First step: number of mu_opt equal to mu_min
% sort M in ascending order
[G_sort,order]          =  sort(G);

% Change order of elements of mu_optwc,mu_min and cap_const
mu_sopt_sort            = zeros(1,Nu);
mu_min_sort             = zeros(1,Nu);

for i=1:1:Nu
    mu_sopt_sort(1,i)       = mu_sopt(1,order(1,i));
    mu_min_sort(1,i)        = mu_min(1,order(1,i));
end

mu_opt_sort             = zeros(1,Nu);

% Compute the number of mu_i equal to mu_min

% Minimum value of lambda
lambda_min              = P0-Ps;
% Temporary value of mu
mu_temp                 = mu_min_sort;
ind_zero                =1;
for i=1:1:Nu
    if mu_sopt_sort(1,i) > mu_min_sort(1,i),
        % Value of lambda from which mu_min = mu_opt
        lambda              = (m*B/G_sort(1,i)+m*Pmax)*log(1+(Pmax*G_sort(1,i))/B)-m*Pmax;
        
        for j=i:1:Nu
    mu_temp(1,j)               = (cap_const*log(2)/B)*(1/(lambertw(exp(-1)*((G_sort(1,j)*lambda/(B*m)) -1))+1));        end
        
        if sum(mu_temp)<1-err,
            ind_zero            = i;
            break;
        else
            lambda_min          = lambda;
        end
    end
end

if ind_zero>1
    mu_opt_sort(1,1:ind_zero-1) = mu_min_sort(1,1:ind_zero-1);
end

sum_mu_min                      = sum(mu_min_sort(1,1:ind_zero-1));
sum_Newton                      = 10;
% Previous and current value of lambda
lambda_cur                      = lambda_min;

% Service time for users whose service time is not minimum
mu_opt_N                        = zeros(1,Nu-ind_zero+1);
der_mu_opt                      = zeros(1,Nu-ind_zero+1);
nb_iter                         = 0;
while sum_Newton>1,
    
    % Newton's method
    
    % Computation of the wlambert function for all users
    for i=ind_zero:1:Nu
        lambert_current                 = lambertw(exp(-1)*((G_sort(1,i)*lambda_cur/(B*m)) -1));
        mu_opt_N(1,i-ind_zero+1)        = (cap_const*log(2)/B)*(1/(lambert_current+1));
        der_mu_opt(1,i-ind_zero+1)      = ((exp(-1)*cap_const*log(2)*G_sort(1,i))/(B^2*m))*exp(-lambert_current)/((lambert_current+1)^3);
    end
    
    lambda_cur                      = lambda_cur + (sum_mu_min+sum(mu_opt_N)-1+err)/sum(der_mu_opt);
    nb_iter                         = nb_iter+1;
    sum_Newton                      = sum_mu_min+sum(mu_opt_N);
end

for i = ind_zero:1:Nu
   mu_opt_sort(1,i)             = (cap_const*log(2)/B)*(1/(lambertw(exp(-1)*((G_sort(1,i)*lambda_cur/(B*m)) -1))+1));
end
% Reorder mu
mu_opt                  = zeros(1,Nu);

for i=1:1:Nu
    mu_opt(1,order(1,i))    = mu_opt_sort(1,i);
end

end